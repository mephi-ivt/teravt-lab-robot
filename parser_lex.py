#!/usr/bin/env python
"""
A simple lex script with Flex syntax
"""

lexis = r"""
    %{
    //int yylineno = 0;
    #include <stdio.h>
    #include <string.h>
    #include "Python.h"
    #define YYSTYPE void *
    #include "tokens.h"
    extern void *py_parser;
    extern void (*py_input)(PyObject *parser, char *buf, int *result,
                            int max_size);
    #define returntoken(tok) \
            yylval = PyString_FromString(strdup(yytext)); return (tok);
    #define YY_INPUT(buf,result,max_size) { \
        (*py_input)(py_parser, buf, &result, max_size); \
    }
    %}

    %%

    [ \t\v\f]             {}
    [\n]                  { yylineno++; }
                    
    "UINT"|"CUINT"        { returntoken(VARDEFINITION); }
    "BOOLEAN"|"CBOOLEAN"  { returntoken(VARDEFINITION); }

    "1DARRAYOFINT"        { returntoken(ARRAYDEFINITION); }
    "2DARRAYOFINT"        { returntoken(ARRAYDEFINITION); }
    "1DARRAYOFBOOL"       { returntoken(ARRAYDEFINITION); }
    "2DARRAYOFBOOL"       { returntoken(ARRAYDEFINITION); }

    "EXTEND1"             { returntoken(EXTENDONE); }
    "EXTEND2"             { returntoken(EXTENDTWO); }
    "SIZE1"               { returntoken(SIZEONE); }
    "SIZE2"               { returntoken(SIZETWO); }

    "WHILE"               { returntoken(WHILE); }
    "DO"                  { returntoken(DO); }
    "IF"                  { returntoken(IF); }
    "ELSE"                { returntoken(ELSE); }

    "INC"|"DEC"           { returntoken(AONEOPERATOR); }
    "+="|"-="|"*="|"/="   { returntoken(ATWOOPERATOR); }

    "NOT"                 { returntoken(LONEOPERATOR); }
    "OR"|"AND"            { returntoken(LTWOOPERATOR); }

    "GT"|"LT"|"ET"        { returntoken(COMPTWOOPERATOR); }

    "FORW"|"BACK"|"LEFT"|"RIGHT"    { returntoken(ROBOTMOVE); }
    "GETF"|"GETB"|"GETL"|"GETR"     { returntoken(ROBOTGET); }
    "PUSHF"|"PUSHB"|"PUSHL"|"PUSHR" { returntoken(ROBOTPUSH); }
    "UNDO"                          { returntoken(ROBOTUNDO); }
    "FINISH"                        { returntoken(ROBOTFINISH); }

    "FUNCTION"            { returntoken(FUNCTION); }
    "OUT"|"PRINT"         { returntoken(OUT); }
    "ENDL"                { returntoken(ENDL); }
    "RBT"                 { returntoken(RBT); }
    "ENT"                 { returntoken(ENT); }

    ([-]|"")[0-9]+                { returntoken(INTVAL); }
    "TRUE"|"FALSE"        { returntoken(BOOLVAL); }
    [a-z][a-zA-Z_0-9]*    { returntoken(LABEL); } 

    [-+*/^(){}=;\,\[\]]   { return(*yytext); } 

    .                     { printf("Bad char\n"); } 

    %%

    yywrap() { return(1); }
    """