#!/usr/bin/env python
from bison import BisonParser
import parser_lex
import tree
from variables import Types
import environment
import robot


class Parser(BisonParser):
    """
    Implements the special Robot Language Parser.
    """

    def __init__(self, **kw):
        self._is_debug = kw.get('debug', False)
        BisonParser.__init__(self, **kw)

        self._env = environment.Environment()

        size = kw.get('size', (8, 8))
        self._env.robot = robot.RobotEnvironment(size[0], size[1])

        field = kw.get('field', None)
        if field is not None:
            self._env.robot.load(field)

        self.file_line = 0
        self.file_pos = 0
    # ==============
    # === Tokens ===
    # ==============
    tokens = ['VARDEFINITION', 'ARRAYDEFINITION',
              # operators
              'AONEOPERATOR', 'ATWOOPERATOR',
              'LONEOPERATOR', 'LTWOOPERATOR', 'COMPTWOOPERATOR',
              'EXTENDONE', 'EXTENDTWO', 'SIZEONE', 'SIZETWO',
              # transitions
              'WHILE', 'DO',
              'IF', 'ELSE',
              'FUNCTION',
              # final definitions
              'INTVAL', 'BOOLVAL', 'LABEL',
              # commands
              'ROBOTMOVE', 'ROBOTGET', 'ROBOTPUSH', 'ROBOTUNDO', 'ROBOTFINISH',
              'OUT', 'ENDL', 'RBT', 'ENT',
              'NL'
              ]

    # ===================
    # === Precedences ===
    # ===================
    precedences = (
        ('left', ('LABEL',)),
        ('left', ('\'(\'', '\'[\'')),

        ('left', ('\'+\'', '\'-\'')),
        ('left', ('\'*\'', '\'/\'')),
        ('left', ('COMPTWOOPERATOR', 'LTWOOPERATOR')),
        ('left', ('LONEOPERATOR',)),

        ('left', ('\')\'',)),
        ('left', ('ELSE',)),
        )

    # =============
    # === Lexis ===
    # =============
    lexscript = parser_lex.lexis

    # ===============
    # === Grammar ===
    # ===============

    # Declare the start target here (by name)
    start = "start"
    debug = False
    interactive = True
    has_errors = False

    def on_start(self, target, option, names, values):
        """
        start : program
        """
        if option == 0:
            if values[0] is not None and not self.has_errors:
                res = values[0].make_tree(self._env)
                print("Program has finished.")
            else:
                print("Program has syntax errors.")

    def handle_errors(self, msg):
        self.has_errors = True
        print(msg)

    def on_program(self, target, option, names, values):
        """
        program : program sentgroup ';'
                | sentgroup ';'
                | NL
        """
        self.file_pos = self.file.tell()
        self.file_line += 1
        if option == 0:
            res_tree = tree.ArrayAppendTree(values[0], values[1])
            return res_tree
        elif option == 1:
            res_tree = tree.SmallArrayTree(values[0], tree.NoneLeaf())
            return res_tree

    def on_sentence(self, target, option, names, values):
        """
        sentence : vardef
                 | operator
                 | function
                 | transition
                 | output
        """
        if option == 0:
            self._debug("def")
            return values[0]
        elif option == 1:
            self._debug("oper")
            return values[0]
        elif option == 2:
            self._debug("func")
            return values[0]
        elif option == 3:
            self._debug("transition")
            return values[0]
        elif option == 4:
            self._debug("out")
            return values[0]
        elif option == 5:
            self._debug("command")
            return values[0]
        else:
            print("Strange!" + str(option))
        return tree.NoneLeaf()

    def on_sentgroup(self, target, option, names, values):
        """
        sentgroup : '{' program '}'
                  | sentence
        """
        if option == 0:
            # TODO: some tree for program
            self.file_line += 2
            self.file_pos = self.file.tell()
            return values[1]
        elif option == 1:
            return values[0]

    # === Definitions ===

    def on_vardef(self, target, option, names, values):
        """
        vardef : VARDEFINITION LABEL '=' expr
               | ARRAYDEFINITION LABEL '=' '[' twodarraydef ']'
               | ARRAYDEFINITION LABEL '=' '[' ']'
        """
        var_name = tree.ValueLeaf(str(values[1]), environment.Types.STR)
        if option == 0:
            available_values = {
                "UINT": tree.VarIntTree, "CUINT": tree.VarCIntTree,
                "BOOLEAN": tree.VarBoolTree, "CBOOLEAN": tree.VarCBoolTree
            }
            if values[0] in available_values:
                var_value = values[3]
                res_tree = available_values[values[0]](var_name, var_value)
                return res_tree
        else:
            array_trees =  {
                "1DARRAYOFINT": tree.Var1DIntArrTree,
                "1DARRAYOFBOOL": tree.Var1DBoolArrTree,
                "2DARRAYOFINT": tree.Var2DIntArrTree,
                "2DARRAYOFBOOL": tree.Var2DBoolArrTree
            }
            if values[0] in array_trees:
                if option == 1:
                    var_value = values[4]
                else:
                    var_value = tree.ArrayAppendTree(tree.NoneLeaf(), tree.NoneLeaf())
                res_tree = array_trees[values[0]](var_name, var_value)
                return res_tree

        return None

    def on_twodarraydef(self, target, option, names, values):
        """
        twodarraydef : twodarraydef ';' defvars
                     | defvars
        """
        if option == 0:
            res_tree = tree.ArrayAppendTree(values[0], values[2])
            return res_tree
        elif option == 1:
            temp_tree = tree.SmallArrayTree(values[0], tree.NoneLeaf())
            return temp_tree

    # === Operators ===

    def on_operator(self, target, option, names, values):
        """
        operator : LABEL '=' expr
                 | LABEL '(' defvars ')' '=' expr
                 | aoperator
                 | arroperator
        """
        if option == 0:
            caption = tree.ValueLeaf(values[0], environment.Types.STR)
            res_tree = tree.EqualOperatorTree(caption, values[2])
            return res_tree
        elif option == 1:
            caption = tree.ValueLeaf(values[0], environment.Types.STR)
            res_tree = tree.ArrEqualOperatorTree(caption, values[2], values[5])
            return res_tree
        elif option == 2:
            return values[0]
        elif option == 3:
            return values[0]

    def on_aoperator(self, target, option, names, values):
        """
        aoperator : AONEOPERATOR LABEL
                  | LABEL ATWOOPERATOR LABEL
        """
        if option == 0:
            caption = tree.ValueLeaf(values[1], environment.Types.STR)
            trees = {
                "INC": tree.IncTree,
                "DEC": tree.DecTree
            }
            if values[0] in trees:
                return trees[values[0]](caption)
            return False

    def on_arroperator(self, target, option, names, values):
        """
        arroperator : EXTENDONE LABEL expr
                    | EXTENDTWO LABEL expr expr
        """
        caption = tree.ValueLeaf(values[1], environment.Types.STR)
        if option == 0:
            res_tree = tree.ArrExtendTree(caption, values[2])
        else:
            res_tree = tree.ArrArrExtendTree(caption, values[2], values[3])
        return res_tree

    # === Functions ===

    def on_function(self, target, option, names, values):
        """
        function : funcdefvars FUNCTION LABEL '(' fulldefvars ')' sentgroup
                 | '[' funcoutvars ']' '=' unigetter
        """
        if option == 0:
            label = tree.ValueLeaf(values[2], Types.STR)
            res_tree = tree.FuncVarTree(values[0], label, values[4], values[6])
            return res_tree
        elif option == 1:
            res_tree = tree.FuncResultsTree(values[1], values[4])
            return res_tree

    def on_funcdefvars(self, target, option, names, values):
        """
        funcdefvars : funcdefonevar
                    | '[' fulldefvars ']'
        """
        if option == 0:
            res_tree = tree.SmallArrayTree(values[0], tree.NoneLeaf())
            return res_tree
        elif option == 1:
            return values[1]

    def on_fulldefvars(self, target, option, names, values):
        """
        fulldefvars : fulldefvars ',' funcdefonevar
                    | funcdefonevar
        """
        if option == 0:
            res_tree = tree.ArrayAppendTree(values[0], values[2])
            return res_tree
        elif option == 1:
            res_tree = tree.SmallArrayTree(values[0], tree.NoneLeaf())
            return res_tree

    def on_funcdefonevar(self, target, option, names, values):
        """
        funcdefonevar : LABEL '=' expr
                      | LABEL '=' error
        """
        if option == 0:
            label = tree.ValueLeaf(values[0], Types.STR)
            res_tree = tree.FuncVarDefTree(label, values[2])
            return res_tree
        if option == 1:
            label = tree.ValueLeaf(values[0], Types.STR)
            res_tree = tree.FuncVarDefTree(label, tree.NoneLeaf())
            msg = "Syntax error: line %d:%d. Bad function vars definition near %s" \
                  % (self.file_line, (self.file.tell() - self.file_pos) / 2, self.last_error)
            self.handle_errors(msg)
            return res_tree

    # === Transitions ===

    def on_transition(self, target, option, names, values):
        """
        transition : WHILE '(' expr ')' DO sentgroup
                   | IF '(' expr ')' sentgroup
                   | IF '[' expr ']' sentgroup
                   | IF '(' expr ')' sentgroup ELSE sentgroup
                   | IF '(' expr ')' ELSE sentgroup
        """
        res_tree = tree.NoneLeaf()
        if option == 0:
            res_tree = tree.WhileTree(values[2], values[5])
        elif option == 1:
            res_tree = tree.IfConditionTree(values[2], values[4], tree.NoneLeaf())
        elif option == 2:
            res_tree = tree.IfConditionTree(values[2], values[4], tree.NoneLeaf())
            msg = "Syntax error: line %d:%d. Bad quotes for IF near %s" \
                  % (self.file_line, (self.file.tell() - self.file_pos) / 2, values[0])
            self.handle_errors(msg)
            return res_tree
        elif option == 3:
            res_tree = tree.IfConditionTree(values[2], values[4], values[6])
        elif option == 4:
            msg = "Syntax error: line %d:%d. ELSE before action for IF near %s" \
                  % (self.file_line, (self.file.tell() - self.file_pos) / 2, values[4])
            self.handle_errors(msg)
            res_tree = tree.IfConditionTree(values[2], values[5], tree.NoneLeaf())
        return res_tree

    # === Base ===

    def on_expr(self, target, option, names, values):
        """
        expr : exprfactor
             | expr '+' expr
             | expr '-' expr
             | expr '*' expr
             | expr '/' expr
             | expr COMPTWOOPERATOR expr
             | expr LTWOOPERATOR expr
             | LONEOPERATOR exprfactor
        """
        if option == 0:
            return values[0]
        elif 1 <= option <= 4:
            op_dict = {
                1: tree.PlusTree, 2: tree.MinusTree,
                3: tree.MultiplyTree, 4: tree.DivideTree,
            }
            res = op_dict[option](values[0], values[2])
            return res
        elif 5 <= option <= 6:
            op_dict = {
                "GT": tree.GreaterTree, "LT": tree.LowerTree, "ET": tree.EqualTree,
                "OR": tree.OrTree, "AND": tree.AndTree,
            }
            if values[1] in op_dict:
                res = op_dict[values[1]](values[0], values[2])
                return res
        elif option == 7:
            res = tree.NotTree(values[1])
            return res
        elif option == 8:
            print('!!!!!')

        return None

    def on_exprfactor(self, target, option, names, values):
        """
        exprfactor : INTVAL
                   | BOOLVAL
                   | getter
                   | command
                   | '(' expr ')'
                   | '{' expr '}'
                   | '[' twodarraydef ']'
        """
        if option == 0:
            return tree.ValueLeaf(int(values[0]))
        elif option == 1:
            return tree.ValueLeaf(values[0] == "TRUE", Types.BOOL)
        elif option == 2:
            return values[0]
        elif option == 3:
            return values[0]
        elif option == 4:
            return values[1]
        elif option == 5:
            msg = "Syntax error: line %d:%d. Bad quotes for expression, use '(' near %s" \
                  % (self.file_line, (self.file.tell() - self.file_pos) / 2, values[0])
            self.handle_errors(msg)
            return values[1]
        elif option == 6:
            return values[1]

    def on_getter(self, target, option, names, values):
        """
        getter : LABEL
               | arrgetter
               | unigetter
        """
        if option == 0:
            return tree.VarLeaf(values[0])
        elif option == 1:
            return values[0]
        elif option == 2:
            return values[0]

    def on_arrgetter(self, target, option, names, values):
        """
        arrgetter : SIZEONE LABEL
                  | SIZETWO LABEL expr
        """
        caption = tree.ValueLeaf(values[1], environment.Types.STR)
        if option == 0:
            res = tree.ArrSizeTree(caption)
            return res
        elif option == 1:
            res = tree.ArrArrSizeTree(caption, values[2])
            return res

    def on_unigetter(self, target, option, names, values):
        """
        unigetter : LABEL '(' defvars ')'
                  | LABEL '[' defvars ']'
                  | LABEL '(' ')'
        """
        caption = tree.ValueLeaf(values[0], environment.Types.STR)
        if option == 0:
            res_tree = tree.UniGetterTree(caption, values[2])
        elif option == 1:
            msg = "Syntax error: line %d:%d. Bad quotes for array getter, use '(' near %s" \
                  % (self.file_line, (self.file.tell() - self.file_pos) / 2, values[0])
            self.handle_errors(msg)
            res_tree = tree.UniGetterTree(caption, values[2])
        elif option == 2:
            res_tree = tree.UniGetterTree(caption, tree.SmallArrayTree(tree.NoneLeaf(), tree.NoneLeaf()))
        return res_tree

    def on_defvars(self, target, option, names, values):
        """
        defvars : defvars ',' expr
                | defvars ',' ','
                | expr
        """
        # TODO: add handler for option == 1
        if option == 0:
            res_tree = tree.ArrayAppendTree(values[0], values[2])
            return res_tree
        elif option == 2:
            res_tree = tree.SmallArrayTree(values[0], tree.NoneLeaf())
            return res_tree

    def on_funcoutvars(self, target, option, names, values):
        """
        funcoutvars : funcoutvars ',' LABEL
                    | funcoutvars ',' ','
                    | LABEL
        """
        # TODO: add handler for option == 1
        if option == 0:
            label = tree.ValueLeaf(str(values[2]), environment.Types.STR)
            res_tree = tree.ArrayAppendTree(values[0], label)
            return res_tree
        elif option == 2:
            label = tree.ValueLeaf(str(values[0]), environment.Types.STR)
            res_tree = tree.SmallArrayTree(label, tree.NoneLeaf())
            return res_tree

    def on_command(self, target, option, names, values):
        """
        command : ROBOTMOVE
                | ROBOTGET
                | ROBOTPUSH
                | ROBOTUNDO
                | ROBOTFINISH
        """
        if option == 3:
            return tree.RobotUndoTree()
        if option == 4:
            return tree.RobotFinishTree()
        if option == 0:
            c_dict = {"FORW": 0, "RIGHT": 1, "BACK": 2, "LEFT": 3}
            res_tree = tree.RobotMoveTree
        elif option == 1:
            c_dict = {"GETF": 0, "GETR": 1, "GETB": 2, "GETL": 3}
            res_tree = tree.RobotGetTree
        else:
            c_dict = {"PUSHF": 0, "PUSHR": 1, "PUSHB": 2, "PUSHL": 3}
            res_tree = tree.RobotPushTree
        return res_tree(c_dict[values[0]])

    def on_output(self, target, option, names, values):
        """
        output : OUT expr
               | ENDL
               | ENT
               | RBT
        """
        if option == 0:
            if values[0] != "OUT":
                msg = "Syntax error: line %d:%d. Here we print with OUT not PRINT near %s" \
                      % (self.file_line, (self.file.tell() - self.file_pos) / 2, values[0])
                self.handle_errors(msg)
            res_tree = tree.OutputTree(values[1])
        elif option == 1:
            res_tree = tree.EndlTree()
        elif option == 2:
            res_tree = tree.EnterTree()
        else:
            res_tree = tree.RobotOutputTree()

        return res_tree

    # ==============
    # === SYSTEM ===
    # ==============

    def _debug(self, message):
        if self._is_debug:
            print("DBG: " + str(message))

    def report_syntax_error(self, msg, yytext, first_line, first_col, last_line, last_col):
        self.last_error = yytext
        self.has_errors = True

if __name__ == '__main__':
    p = Parser()
    p.run()