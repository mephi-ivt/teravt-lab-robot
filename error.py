class BaseError:
    """
    Base error class
    """

    _message = "Err: Unknown error!"
    _code = 0
    _is_warning = False

    def __init__(self, message=None, code=None, warning=None):
        if message is not None and type(code) is str:
            self._message = message
        if code is not None and type(code) is int:
            self._code = code
        if warning is not None and type(code) is bool:
            self._is_warning = warning

    def get_message(self):
        """
        Returns error message
        :return: str
        """
        return self._message

    def get_code(self):
        """
        Returns error code
        :return: int
        """
        return self._code

    def is_warning(self):
        """
        Returns True if error is not so important
        :return: boolean
        """
        return self._is_warning

    def __str__(self):
        return self.get_message()

    def __int__(self):
        return self.get_code()


class BaseWarning(BaseError):
    """
    Warning means this error error doesn't affect the higher parts of the tree
    """
    _message = "Wrn: Unknown warning!"
    _is_warning = True

    def __init__(self, message=None, code=None):
        BaseError.__init__(self, message, code)
        if message is not None and type(code) is str:
            self._message = message
        if code is not None and type(code) is int:
            self._code = code


class ChildWarning(BaseWarning):
    """
    This warning means tree children has some warnings
    """
    _message = "Wrn: Tree children have warning!"
    _code = -2


class ChildError(BaseError):
    """
    This error means tree children has some errors
    """
    _message = "Err: Tree children have error!"
    _code = -1


class CustomError(BaseError):
    """
    Virtual class for handling custom errors.
    In CustomErrors you can change only message.
    """
    def __init__(self, message=None):
        BaseError.__init__(self, message)
        if message is not None:
            self._message = message


class TypeMismatchError(CustomError):
    """
    This error means types of variables are not comparable 
    """
    _message = "Err: Types are not comparable!"
    _code = 1
    _is_warning = False


class NoNameError(CustomError):
    """
    You tried to get access to the variable that does not exist
    """
    _message = "Err: NameError! No variables with this name!"
    _code = 2
    _is_warning = False


class AlreadyDefineError(CustomError):
    """
    You tried to define variable that already exists
    """
    _message = "Err: AlreadyDefined!"
    _code = 3
    _is_warning = False


class ArraySizeError(CustomError):
    _message = "Err: Array size is wrong"
    _code = 4
    _is_warning = False


class ConstError(CustomError):
    _message = "Err: Variable is const"
    _code = 5
    _is_warning = False


class FuncVarsError(CustomError):
    _message = "Err: All vars in function should have one type"
    _code = 6
    _is_warning = False


class TooMuchFuncVarsError(CustomError):
    _message = "Err: You try to pass too much values in function"
    _code = 7
    _is_warning = False
