# coding: utf8
import math


class RobotEnvironment:
    """
    All environment for the robot
    It has hole information about a field, walls and robot position
    """
    _objects = {}
    _exits = [[], [], [], []]  # right, down, left, up
    _direction = 0  # 0-right, 1-down, 2-left, 3-up
    _commands = []

    _finished = False
    _last_move = False

    _directions = {0: (1, 0), 1: (0, 1), 2: (-1, 0), 3: (0, -1)}
    counter = 0

    def __init__(self, size_x, size_y, robot_x=0, robot_y=0, walls=[], exits=[]):
        self._size = (size_x, size_y)
        self._robot = (robot_x, robot_y)

    def is_finished(self):
        return self._finished

    def has(self, x, y):
        if (x, y) in self._objects:
            return self._objects[(x, y)]
        return None

    def load(self, filename):
        f = open(filename, 'r')
        res = []
        for full_line in f:
            line = list(map(int, full_line[:-1].split(' ')))
            res.append(line)
        self._size = (res[0][0], res[0][1])
        self._robot = (res[1][0], res[1][1])
        for i in range(0, len(res[3]), 2):
            self._objects[(res[3][i], res[3][i+1])] = Wall()
        for i in range(0, len(res[2]), 2):
            self._exits[res[2][i]].append(res[2][i+1])

    def _correct_direction(self, direction=None):
        if direction is None:
            direction = self._direction
        while direction > 3:
            direction -= 4
        while direction < 0:
            direction += 4
        return direction

    def _is_out(self, x, y):
        return x >= self._size[0] or y >= self._size[1] or x < 0 or y < 0

    def _is_finished(self, x, y):
        is_out = self._is_out(x, y)
        if not is_out:
            return False
        if x == -1:
            exits, coord = self._exits[2], y  # left
        elif x == self._size[0]:
            exits, coord = self._exits[0], y  # right
        elif y == -1:
            exits, coord = self._exits[3], x  # top
        else:
            exits, coord = self._exits[1], x  # bottom
        return coord in exits

    def _direction_exits(self, direction=None):
        if direction is None or not 0 <= direction <= 3:
            direction = self._direction
        x, y = self._robot
        all_exits = {
            0: [self._exits[0], self._size[0] - x, y], 1: [self._exits[1], self._size[1] - y, x],
            2: [self._exits[2], x + 1, y], 3: [self._exits[3], y + 1, x]
        }
        exits = all_exits[direction]
        if len(exits[0]) == 0:
            return self._size[0] * self._size[1]
        min_dist = self._size[0] * self._size[1]
        for _exit in exits[0]:
            dist = int(math.sqrt((exits[1]**2 + (_exit - exits[2])**2)))
            if dist < min_dist:
                min_dist = dist
        return min_dist

    def rotate(self, direction=1, times=1):
        """
        Rotate robot (-1 is left, 1 is right)
        :param direction: int
        :param times: int (how much rotations in this direction)
        :return: True
        """
        self._direction = self._virtual_rotate(direction, times)

    def _virtual_rotate(self, direction=1, times=1, direct=None):
        if direct is None:
            direct = self._direction
        for i in range(0, times):
            direct += direction
        direct = self._correct_direction(direct)
        return direct

    def go(self, direction=None):
        self._last_move = False
        self.counter += 1
        new_pos = self._near_pos(direction)
        some_object = new_pos in self._objects
        end_of_field = self._is_out(new_pos[0], new_pos[1])
        finished = self._is_finished(new_pos[0], new_pos[1])
        if some_object or (end_of_field and not finished):
            return False
        self._robot = new_pos
        if finished:
            self._finished = True
        else:
            self._finished = False
        return True

    def _move_block(self, x, y, direction=None):
        if not self.has(x, y):
            return False
        new_pos = self._near_pos(direction, x, y)
        some_object = new_pos in self._objects
        end_of_field = self._is_out(new_pos[0], new_pos[1])
        if some_object or end_of_field:
            return False
        self._objects[new_pos] = self._objects.pop((x, y))
        self._commands.append(new_pos)
        self._last_move = True
        return True

    def _near_pos(self, direction=None, x=None, y=None):
        if x is None or y is None:
            x, y = self._robot
        if direction is None:
            direction = self._direction
        math_direction = self._directions[direction]
        new_pos = (x + math_direction[0], y + math_direction[1])
        return new_pos

    # == ACTIONS ==

    def action_forw(self):
        return self.go()

    def action_back(self):
        self.rotate(times=2)
        return self.go()

    def action_right(self):
        self.rotate(1)
        return self.go()

    def action_left(self):
        self.rotate(-1)
        return self.go()

    def action_getf(self):
        return self._direction_exits()

    def action_getb(self):
        return self._direction_exits(self._virtual_rotate(1, 2))

    def action_getr(self):
        return self._direction_exits(self._virtual_rotate(1))

    def action_getl(self):
        return self._direction_exits(self._virtual_rotate(-1))

    def action_pushf(self):
        near_pos = self._near_pos()
        return self._move_block(near_pos[0], near_pos[1])

    def action_pushb(self):
        self.rotate(times=2)
        near_pos = self._near_pos()
        return self._move_block(near_pos[0], near_pos[1])

    def action_pushr(self):
        self.rotate(1)
        near_pos = self._near_pos()
        return self._move_block(near_pos[0], near_pos[1])

    def action_pushl(self):
        self.rotate(-1)
        near_pos = self._near_pos()
        return self._move_block(near_pos[0], near_pos[1])

    def action_undo(self):
        if not self._last_move:
            return False
        else:
            self._last_move = False
            pos = self._commands[-1]
            return self._move_block(pos[0], pos[1], self._virtual_rotate(times=2))

    def draw(self):
        w, h = self._size
        final_str = "#"
        for i in range(0, w):
            if self._is_finished(i, -1):
                if self._robot == (i, -1):
                    final_str += "8"
                else:
                    final_str += "o"
            else:
                final_str += "#"
        final_str += "#\n"
        for i in range(0, h):
            if i in self._exits[2]:
                if self._robot == (-1, i):
                    final_str += "8"
                else:
                    final_str += "o"
            else:
                final_str += "#"
            for j in range(0, w):
                t_str = " "
                if self.has(j, i):
                    t_str = "*"
                elif (j, i) == self._robot:
                    icon = {0: u'→', 1: u'↓', 2: u'←', 3: u'↑'}
                    t_str = icon[self._direction]
                final_str += t_str
            if i in self._exits[0]:
                if self._robot == (w, i):
                    final_str += "8\n"
                else:
                    final_str += "o\n"
            else:
                final_str += "#\n"

        final_str += "#"
        for i in range(0, w):
            if self._is_finished(i, h):
                if self._robot == (i, h):
                    final_str += "8"
                else:
                    final_str += "o"
            else:
                final_str += "#"
        final_str += "#\n"
        return final_str[:-1]

    def __getitem__(self, key):
        if key > 1 or key < 0:
            return None
        return self._robot[key]


class Wall:
    """
    Just a wall. Absolutely useless.
    """
    def __init__(self):
        self._type = 0
