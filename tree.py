from __future__ import print_function
from variables import *
import validators
import error
import copy


class OperationTypes:
    """
    Class that handles all types of the operations.
    Now you can easily know BaseTree operation with 'get_operation' method
    """
    # Operations and basic stuff
    BASIC, VALUE, VARIABLE, NONETYPE = 'BASIC', 'VALUE', 'VARIABLE', 'NONETYPE'
    PLUS, MINUS, MULTIPLY, DIVIDE = 'PLUS', 'MINUS', 'MULTIPLY', 'DIVIDE'
    GREATER, LOWER, OR, AND, NOT = 'GREATER', 'LOWER', 'OR', 'AND', 'NOT'
    # Vars
    VAR, UINT, CUINT, BOOL, CBOOL = 'VAR', 'UINT', 'CUINT', 'BOOL', 'CBOOL'
    ARR, ARRCONCAT, TEMPARR = 'ARR', 'ARRCONCAT', 'TEMPARR'
    ARRINT, ARRBOOL, ARR2INT, ARR2BOOL = 'ARRINT', 'ARRBOOL', 'ARR2INT', 'ARR2BOOL'

    UNIGETTER = 'UNIGETTER'

    PROGRAM_BLOCK = 'PROGRAM_BLOCK'
    IF_CONDITION, WHILE_CYCLE = 'IF_CONDITION', 'WHILE_CYCLE'
    FUNC_DEF = 'FUNC_DEF'

    ROBOT_MOVE, ROBOT_GET, ROBOT_PUSH = 'ROBOT_MOVE', 'ROBOT_GET', 'ROBOT_PUSH'
    ROBOT_UNDO, ROBOT_FINISH = 'ROBOT_UNDO', 'ROBOT_FINISH'

    OUTPUT = 'OUTPUT'

# =============
# === TREES ===
# =============


class BaseTree:
    """
    Base Tree class. Trees should be created by Robot Parser.
    """

    _left = None
    _right = None
    _operation_num = OperationTypes.BASIC

    _errors = []

    def __init__(self, left=None, right=None, operation=None):
        self._left = left
        self._right = right
        if operation is not None:
            self._operation_num = operation

    def get_operation(self):
        """
        Returns operation code
        :return: int
        """
        return self._operation_num

    def is_leaf(self):
        """
        Shows if tree element is Leaf (has no children)
        :return: boolean
        """
        return self._left is None and self._right is None

    def make_tree(self, env):
        """
        Recursively collects a tree
        :param env: current Environment object
        :return: result of making Tree
        """
        self._clean_tree()

        left_res = self._left.make_tree(env)
        right_res = self._right.make_tree(env)

        self._add_child_errors(self._left, 'left')
        self._add_child_errors(self._right, 'right')
        if len(self.errors()) == 0 and self._validation(left_res, right_res, env):
                return self._operation(self, left_res, right_res, env=env)
        return ResultVariable(False, "BaseOperationOutput failed")

    def _clean_tree(self):
        """
        Cleans tree from previous usage (for example, errors)
        :return: 
        """
        self._errors = []

    def _validation(self, left, right, env=None):
        """
        This method should validate variables compatibility and create errors if need
        :param left: Tree result
        :param right: Tree result
        :param env: current Environment with all the information
        :return: boolean
        """
        return True

    @staticmethod
    def _operation(tree, left, right, env=None):
        """
        :param tree: current BaseTree children object
        :param left: BaseTree children making result
        :param right: BaseTree children making result
        :param env: current Environment with all the information
        :return: tree result
        """
        return ResultVariable(True, "BaseOperationOutput")

    def errors(self):
        """
        Method copies list with errors and returns it
        :return: list with BaseError children
        """
        return copy.copy(self._errors)

    def _add_error(self, adding_error, notify=True):
        """
        Ads error to error list
        :param adding_error: BaseError children
        :return: 
        """
        self._errors.append(adding_error)
        if notify:
            print("ERROR: " + str(adding_error))

    def _add_child_errors(self, tree_with_errors, comment=None):
        """
        Add errors from tree children
        :param tree_with_errors: BaseTree children
        :param comment: str with comment (usually left or right)
        :return: 
        """
        for err in tree_with_errors.errors():
            _msg = "CHILDREN!" + str(err)
            if comment is not None:
                _msg += " (" + str(comment) + ")"
            if err.is_warning():
                self._add_error(error.ChildWarning(_msg), False)
            else:
                self._add_error(error.ChildError(_msg), False)


class BaseHalfTree(BaseTree):
    """
    Tree with only one operand.
    Value is kept in left part of BaseTree
    _operation here is _half_operation and _validation is _half_validation
    """

    def __init__(self, value=None, operation=None):
        BaseTree.__init__(self, value, None, operation)

    def _half_validation(self, value, env=None):
        return True

    @staticmethod
    def _half_operation(tree, value, env=None):
        return ResultVariable(True, "BaseHalfOperationOutput")

    def make_tree(self, env):
        self._clean_tree()

        res = self._left.make_tree(env)
        self._add_child_errors(self._left)
        if len(self.errors()) == 0 and self._half_validation(res, env):
                return self._half_operation(self, res, env=env)
        return ResultVariable(False, "BaseHalfOperationOutput failed")


# === Arithmetical ===


class ArithmeticalTree(BaseTree):
    """
    Class that handles numbers
    """
    def _validation(self, left, right, env=None):
        BaseTree._validation(self, left, right, env)
        _error = validators.two_int_validator(left, right)
        if _error is not None:
            self._add_error(_error)
            return False
        return True


class PlusTree(ArithmeticalTree):
    def __init__(self, left, right):
        BaseTree.__init__(self, left, right, OperationTypes.PLUS)

    @staticmethod
    def _operation(tree, left, right, env=None):
        return IntVariable(left.value() + right.value())


class MinusTree(ArithmeticalTree):
    def __init__(self, left, right):
        BaseTree.__init__(self, left, right, OperationTypes.MINUS)

    @staticmethod
    def _operation(tree, left, right, env=None):
        return IntVariable(left.value() - right.value())


class MultiplyTree(ArithmeticalTree):
    def __init__(self, left, right):
        BaseTree.__init__(self, left, right, OperationTypes.MULTIPLY)

    @staticmethod
    def _operation(tree, left, right, env=None):
        return IntVariable(left.value() * right.value())


class DivideTree(ArithmeticalTree):
    def __init__(self, left, right):
        BaseTree.__init__(self, left, right, OperationTypes.DIVIDE)

    @staticmethod
    def _operation(tree, left, right, env=None):
        return IntVariable(left.value() / right.value())

# === Logical ===


class GreaterTree(ArithmeticalTree):
    def __init__(self, left, right):
        BaseTree.__init__(self, left, right, OperationTypes.GREATER)

    @staticmethod
    def _operation(tree, left, right, env=None):
        return BoolVariable(left.value() > right.value())


class LowerTree(ArithmeticalTree):
    def __init__(self, left, right):
        BaseTree.__init__(self, left, right, OperationTypes.LOWER)

    @staticmethod
    def _operation(tree, left, right, env=None):
        return BoolVariable(left.value() < right.value())


class EqualTree(ArithmeticalTree):
    def __init__(self, left, right):
        BaseTree.__init__(self, left, right, OperationTypes.LOWER)

    @staticmethod
    def _operation(tree, left, right, env=None):
        return BoolVariable(left.value() == right.value())


class OrTree(BaseTree):
    def __init__(self, left, right):
        BaseTree.__init__(self, left, right, OperationTypes.OR)

    @staticmethod
    def _operation(tree, left, right, env=None):
        return BoolVariable(left.value() or right.value())


class AndTree(BaseTree):
    def __init__(self, left, right):
        BaseTree.__init__(self, left, right, OperationTypes.AND)

    @staticmethod
    def _operation(tree, left, right, env=None):
        return BoolVariable(left.value() and right.value())


class NotTree(BaseTree):
    def __init__(self, expr):
        BaseTree.__init__(self, expr, NoneLeaf(), OperationTypes.NOT)

    @staticmethod
    def _operation(tree, left, right, env=None):
        return BoolVariable(not left.value())

# =================
# === VARIABLES ===
# =================


class VarTree(BaseTree):
    """
    Necessary to create variables in the environment
    """
    _operation_num = OperationTypes.VAR
    _var_type = None

    @staticmethod
    def _check_environment(env):
        return env is not None  # TODO: Make correct validation of the environment

    def _validation(self, left, right, env=None):
        if env is None:
            return False
        validation = True
        # validate already defined vars
        if env.has_variable(left):
            _msg = "Err: " + str(left) + " is AlreadyDefined!"
            self._add_error(error.AlreadyDefineError(_msg))
            validation = False
        # validate wrong types
        _error = validators.one_type_validator(right, self._var_type)
        if _error is not None:
            self._add_error(_error)
            validation = False

        return validation

    @staticmethod
    def _operation(tree, left, right, env=None):
        if not VarTree._check_environment(env):
            return ResultVariable(False, "Env problems in VarTree")
        res = env.add_variable(left.value(), right)
        return ResultVariable(res, "VarTree result")


class VarIntTree(VarTree):
    """
    Create UINT variables in the environment
    """
    _operation_num = OperationTypes.UINT
    _var_type = Types.INT


class VarCIntTree(VarIntTree):
    """
    Creates CUINT variables in the environment
    """
    _operation_num = OperationTypes.CUINT
    _var_type = Types.INT

    @staticmethod
    def _operation(tree, left, right, env=None):
        if not VarTree._check_environment(env):
            return ResultVariable(False, "Env problems in VarCIntTree")
        right.set_const()
        res = env.add_variable(left.value(), right)
        return ResultVariable(res, "VarCIntTree result")


class VarBoolTree(VarTree):
    """
    Creates BOOLEAN variables in the environment
    """
    _operation_num = OperationTypes.BOOL
    _var_type = Types.BOOL


class VarCBoolTree(VarTree):
    """
    Creates CBOOLEAN variables in the environment
    """
    _operation_num = OperationTypes.CBOOL
    _var_type = Types.BOOL

    @staticmethod
    def _operation(tree, left, right, env=None):
        if not VarTree._check_environment(env):
            return ResultVariable(False, "Env problems in VarCBoolTree")
        right.set_const()
        res = env.add_variable(left.value(), right)
        return ResultVariable(res, "VarCBoolTree result")


class VarArrTree(VarTree):
    """
    Base class to create array variables in the environment
    """
    _operation_num = OperationTypes.ARR
    _var_type = None
    _val_type = None

    def _validation(self, left, right, env=None):
        if env is None:
            return False
        validation = True
        # validate already defined vars
        if env.has_variable(left):
            _msg = "Err: " + str(left) + " is AlreadyDefined!"
            self._add_error(error.AlreadyDefineError(_msg))
            validation = False
        if right is None:
            return validation
        _arr = right.values()[0].values()
        _type_error = validators.type_validator(_arr, self._val_type)
        if _type_error is not None:
            self._add_error(_type_error)
            validation = False

        return validation

    def get_val_type(self):
        return self._val_type

    @staticmethod
    def _operation(tree, left, right, env=None):
        if not VarTree._check_environment(env):
            return ResultVariable(False, "Env problems in VarArrTree")
        if right is None:
            _arr = ArrVariable([], tree.get_val_type())
        else:
            _arr = right.values()[0]
        res = env.add_variable(left.value(), _arr)
        return ResultVariable(res, "VarArrTree result")


class Var1DIntArrTree(VarArrTree):
    """
    Creates 1DARRAYOFINT variables in the environment
    """
    _operation_num = OperationTypes.ARRINT
    _val_type = Types.INT


class Var1DBoolArrTree(VarArrTree):
    """
    Creates 1DARRAYOFBOOL variables in the environment
    """
    _operation_num = OperationTypes.ARRBOOL
    _val_type = Types.BOOL


class Var2DArrTree(VarTree):
    """
    Base class to create array variables in the environment
    """
    _operation_num = OperationTypes.ARR
    _var_type = None
    _val_type = None

    def _validation(self, left, right, env=None):
        if env is None:
            return False
        validation = True
        # validate already defined vars
        if env.has_variable(left):
            _msg = "Err: " + str(left) + " is AlreadyDefined!"
            self._add_error(error.AlreadyDefineError(_msg))
            validation = False
        # validate wrong types
        if right is None:
            return validation
        _type_error = validators.twod_array_validator(right, self._val_type)
        if _type_error is not None:
            self._add_error(_type_error)
            validation = False
        return validation

    def get_val_type(self):
        return self._val_type

    @staticmethod
    def _operation(tree, left, right, env=None):
        if not VarTree._check_environment(env):
            return ResultVariable(False, "Env problems in Var2DArrTree")
        if right is None:
            right = ArrVariable([], Types.ARR)
        res = env.add_variable(left.value(), right)
        return ResultVariable(res, "Var2DArrTree result")


class Var2DIntArrTree(Var2DArrTree):
    """
    Creates 2DARRAYOFINT variables in the environment
    """
    _operation_num = OperationTypes.ARR2INT
    _val_type = Types.INT


class Var2DBoolArrTree(Var2DArrTree):
    """
    Creates 2DARRAYOFBOOL variables in the environment
    """
    _operation_num = OperationTypes.ARR2BOOL
    _val_type = Types.BOOL

# ==============
# === ARRAYS ===
# ==============


class SmallArrayTree(BaseTree):
    """
    Makes array from one element
    """
    _operation_num = OperationTypes.ARRCONCAT

    @staticmethod
    def _operation(tree, left, right, env=None):
        if left is None and right is None:
            return FreeArrVariable([])
        return ArrVariable([left], left.var_type())


class ArrayAppendTree(BaseTree):
    """
    Concats arrays
    """
    _operation_num = OperationTypes.ARRCONCAT

    @staticmethod
    def _operation(tree, left, right, env=None):
        if left is None or right is None:
            return left
        left.arr_append(right)
        return left


# =================
# === OPERATORS ===
# =================

class EqualOperatorTree(BaseTree):
    """
    Operator that set some variable
    """

    def _validation(self, left, right, env=None):
        exist_and_change = validators.changeable_validator(left.value(), env)
        if exist_and_change is not None:
            self._add_error(exist_and_change)
            return False
        val = env.get_variable(left.value())
        correct_types = validators.one_type_validator(right, val.var_type())
        if correct_types is not None:
            self._add_error(correct_types)
            return False
        if right.var_type() == Types.ARR:
            array_correctness = validators.arrays_correctness_validator(left.value(), right, env)
            if array_correctness is not None:
                self._add_error(array_correctness)
                return False
        return True

    @staticmethod
    def _operation(tree, left, right, env=None):
        res = env.change_variable(left.value(), right.value())
        desc = str(left.value()) + ' = ' + str(right.value())
        return ResultVariable(res, desc)


class ArrEqualOperatorTree(BaseTree):
    """
    Operator that set some variable of array
    """
    def __init__(self, label, vars, expr):
        BaseTree.__init__(self)
        self._left = label
        self._right = vars
        self._expr = expr

    def _eq_validation(self, label, vars, expr, env=None):
        exist = validators.existance_validator(label.value(), env)
        if exist is not None:
            self._add_error(exist)
            return False
        arr = env.get_variable(label.value())
        correct_length = validators.array_size_validator(arr, vars[0].value())
        if correct_length is not None:
            self._add_error(correct_length)
            return False
        val = arr[vars[0].value()]
        if len(vars.values()) > 1:
            correct_length = validators.array_size_validator(arr[vars[0].value()], vars[1].value())
            if correct_length is not None:
                self._add_error(correct_length)
                return False
            val = val[vars[1].value()]
        correct_types = validators.one_type_validator(expr, val.var_type())
        if correct_types is not None:
            self._add_error(correct_types)
            return False
        return True

    @staticmethod
    def _eq_operation(tree, label, vars, expr, env=None):
        if len(vars.values()) == 1:
            res = env.change_arr(label.value(), vars[0].value(), expr)
        elif len(vars.values()) == 2:
            res = env.change_arr_arr(label.value(), vars[0].value(), vars[1].value(), expr)
        else:
            return ResultVariable(False, "Error with array changing")
        desc = "Arr changing"
        return ResultVariable(res, desc)

    def make_tree(self, env):
        self._clean_tree()

        label_res = self._left.make_tree(env)
        vars_res = self._right.make_tree(env)
        expr_res = self._expr.make_tree(env)

        self._add_child_errors(self._left, 'label')
        self._add_child_errors(self._right, 'vars')
        self._add_child_errors(self._expr, 'expr')
        if len(self.errors()) == 0 and self._eq_validation(label_res, vars_res, expr_res, env):
            return self._eq_operation(self, label_res, vars_res, expr_res, env)
        return ResultVariable(False, "Array '=' failed")


class ArrSizeTree(BaseHalfTree):
    """
    Returns size of ONED array
    """

    def _half_validation(self, value, env=None):
        exist = validators.existance_validator(value.value(), env)
        if exist is not None:
            self._add_error(exist)
            return False
        return True

    @staticmethod
    def _half_operation(tree, operand, env=None):
        arr = env.get_variable(operand.value())
        return IntVariable(len(arr.values()))


class ArrArrSizeTree(BaseTree):
    """
    Returns size of TWOD array
    """

    _operation_num = OperationTypes.ARRCONCAT

    @staticmethod
    def _operation(tree, left, right, env=None):
        arr = env.get_variable(left.value())
        return IntVariable(len(arr[right.value()].values()))


class ArrExtendTree(BaseTree):
    """
    Extend 1D array
    """

    def _validation(self, left, right, env=None):
        exist = validators.existance_validator(left.value(), env)
        int_size = validators.one_type_validator(right, Types.INT)
        correct_length = None
        if exist is None:
            val = env.get_variable(left.value())
            correct_length = validators.array_extend_validator(val, right.value())
        for err in [exist, int_size, correct_length]:
            if err is not None:
                self._add_error(err)
                return False
        return True

    @staticmethod
    def _operation(tree, left, right, env=None):
        res = env.resize_arr(left.value(), right.value())
        return ResultVariable(res, "Array resizing")


class ArrArrExtendTree(BaseTree):
    """
    Extend 1D array in 2D array
    """

    def __init__(self, caption, index, value):
        BaseTree.__init__(self)
        self._left = caption
        self._right = index
        self._value = value

    def _arr_validation(self, caption, index, value, env=None):
        exist = validators.existance_validator(caption.value(), env)
        int_size = validators.one_type_validator(value, Types.INT)
        correct_length = None
        correct_index = None
        if exist is None:
            val = env.get_variable(caption.value())
            correct_index = validators.array_size_validator(val, index.value())
            if correct_index is None:
                correct_length = validators.array_extend_validator(val[index.value()], value.value())
        for err in [exist, int_size, correct_index, correct_length]:
            if err is not None:
                self._add_error(err)
                return False
        return True

    @staticmethod
    def _arr_operation(tree, caption, index, value, env=None):
        res = env.resize_arr_arr(caption.value(), index.value(), value.value())
        return ResultVariable(res, "Array 2D resizing")

    def make_tree(self, env):
        self._clean_tree()

        caption = self._left.make_tree(env)
        index = self._right.make_tree(env)
        value = self._value.make_tree(env)
        self._add_child_errors(self._left)
        self._add_child_errors(self._right)
        self._add_child_errors(self._value)
        if len(self.errors()) > 0 or not self._arr_validation(caption, index, value, env):
            return ResultVariable(False, "Bad validation in ArrArrResizing")
        res_tree = self._arr_operation(self, caption, index, value, env)
        return res_tree


class UniGetterTree(BaseTree):
    """
    Tree node that can return value of array or function result
    """
    _operation_num = OperationTypes.UNIGETTER

    def __init__(self, left, right):
        BaseTree.__init__(self, left, right)

    def _validation(self, left, right, env=None):
        errors = validators.unigetter_validator(left.value(), right.value(), env)
        if errors is not None:
            self._add_error(errors)
            return False
        return True

    @staticmethod
    def _operation(tree, left, right, env=None):
        if env.has_func(left.value()):
            return env.make_func(left.value(), right.values())
        variables = right.values()
        if len(variables) < 3:
            if len(variables) == 1:
                return env.get_variable(left.value())[variables[0].value()]
            elif len(variables) == 2:
                oned = env.get_variable(left.value())[variables[0].value()]
                return oned[variables[1].value()]
        bad_desc = "Failed to get " + str(left.value)
        return ResultVariable(False, bad_desc)


class IncTree(BaseHalfTree):
    """
    Increments variable
    """

    def _half_validation(self, label, env=None):
        exist = validators.existance_validator(label.value(), env)
        if exist is not None:
            self._add_error(exist)
            return False
        return True

    @staticmethod
    def _half_operation(tree, operand, env=None):
        var = env.get_variable(operand.value())
        res = env.change_variable(operand.value(), var.value() + 1)
        return ResultVariable(res, "Increment")


class DecTree(IncTree):
    """
    Decrements variable
    """

    @staticmethod
    def _half_operation(tree, operand, env=None):
        var = env.get_variable(operand.value())
        res = env.change_variable(operand.value(), var.value() - 1)
        return ResultVariable(res, "Decrement")

# ==============
# === SYSTEM ===
# ==============


class ProgramBlockTree(BaseHalfTree):
    """
    Body for program blocks
    """
    _operation_num = OperationTypes.PROGRAM_BLOCK

# ===================
# === TRANSITIONS ===
# ===================


class IfConditionTree(BaseTree):
    """
    Tree node that manage if-condition
    """
    _operation_num = OperationTypes.IF_CONDITION

    def __init__(self, cond, if_true, if_false):
        """
        :param cond: BaseTree
        :param if_true: BaseTree
        :param if_false: BaseTree
        """
        BaseTree.__init__(self)
        self._cond = cond
        self._left = if_true
        self._right = if_false

    def _validate_condition(self, cond, env=None):
        _error = validators.one_type_validator(cond, Types.BOOL)
        if _error is not None:
            self._add_error(_error)
            return False
        return True

    def _validate_tree_part(self, tree_part, env=None):
        # TODO: make validation
        return True

    def make_tree(self, env):
        self._clean_tree()

        cond = self._cond.make_tree(env)
        self._add_child_errors(self._cond)
        if len(self.errors()) > 0 or not self._validate_condition(cond, env):
            return ResultVariable(False, "Condition in IF has errors")
        res_tree = self._left
        if not cond.value():
            res_tree = self._right
        res = res_tree.make_tree(env)
        self._add_child_errors(res_tree)
        if res is None:
            return ResultVariable(True, "Else wanted but no else")
        return res


class WhileTree(BaseTree):
    """
    Tree node that manage while cycle
    """
    _operation_num = OperationTypes.WHILE_CYCLE

    def __init__(self, cond, action):
        """
        :param cond: BaseTree
        :param action: BaseTree
        """
        BaseTree.__init__(self)
        self._left = cond
        self._right = action

    def _validate_condition(self, cond, env=None):
        _error = validators.one_type_validator(cond, Types.BOOL)
        if _error is not None:
            self._add_error(_error)
            return False
        return True

    def _validate_action(self, action, env=None):
        # TODO: make validation
        return True

    def make_tree(self, env):
        self._clean_tree()

        cond = self._left.make_tree(env)
        self._add_child_errors(self._left)
        if len(self.errors()) > 0 or not self._validate_condition(cond, env):
            return ResultVariable(False, "Condition in IF has errors")
        res_tree = self._right
        if not cond.value():
            return ResultVariable(True, "While was finished")
        res = res_tree.make_tree(env)
        self._add_child_errors(res_tree)
        self.make_tree(env)
        return res

# =================
# === FUNCTIONS ===
# =================


class FuncVarDefTree(BaseTree):
    """
    Keeps name and standard value for function variables
    """
    def _validation(self, left, right, env=None):
        # TODO: add validation
        return True

    @staticmethod
    def _operation(tree, left, right, env=None):
        return FuncDefVariable(left, right)


class FuncVarTree(BaseTree):
    """
    Add function to the Environment memory
    """
    _operation_num = OperationTypes.FUNC_DEF

    def __init__(self, out_vars, label, in_vars, action):
        """
        :param out_vars: BaseTree - array of FuncVarDefTrees for func return
        :param label: ValueLeaf with func label
        :param in_vars: BaseTree - array of FuncVarDefTrees
        :param action: BaseTree with program block
        """
        BaseTree.__init__(self)
        self._left = out_vars
        self._right = in_vars
        self._label = label
        self._action = action

    def _pre_validation(self, out_vars, in_vars, label, env):
        _error = validators.funcs_def_validator(out_vars, in_vars, label.value())
        if _error is not None:
            self._add_error(_error)
            return False
        return True

    def make_tree(self, env):
        self._clean_tree()

        out_vars = self._left.make_tree(env)
        in_vars = self._right.make_tree(env)
        label = self._label.make_tree(env)
        action = Variable(self._action, Types.TREE)
        tree_dict = {'out_vars': self._left, 'in_vars': self._right,
                     'label': self._label, 'action': self._action}
        for tree_label, tree in tree_dict.iteritems():
            self._add_child_errors(tree, tree_label)

        _pre_validation = self._pre_validation(out_vars, in_vars, label, env)
        if len(self.errors()) > 0 or not _pre_validation:
            return ResultVariable(False, "Func pre-validation failed")
        res = env.add_func(out_vars, label, in_vars, action)
        return ResultVariable(res, "Func adding to the memory")


class FuncResultsTree(BaseTree):
    """
    Equals function results to the variables
    """

    @staticmethod
    def _operation(tree, left, right, env=None):
        variables = left.values()
        if right.var_type() != Types.ARR:
            results = [right]
        else:
            results = right.value()

        counter = 0
        for variable in variables:
            if counter < len(results) and env.has_variable(variable.value()):
                env.change_variable(variable.value(), results[counter].value())
            counter += 1

        return FuncDefVariable(left, right)

# =============
# === LEAFS ===
# =============


class ValueLeaf(BaseTree):
    """
    End of tree - usual leaf.
    Value with some type.
    """

    _operation_num = OperationTypes.VALUE
    _type = Types.INT
    _value = None

    def __init__(self, value, var_type=None):
        self._value = Variable(value, var_type)
        if var_type is not None:
            self._type = var_type
        BaseTree.__init__(self)

    def get_value(self):
        return self._value

    def make_tree(self, env):
        self._clean_tree()
        return self.get_value()


class VarLeaf(BaseTree):
    """
    End of tree - usual leaf.
    Handle name of variable and can get it from environment
    """

    _operation_num = OperationTypes.VARIABLE
    _type = Types.INT
    _caption = None

    def __init__(self, caption, var_type=None):
        self._caption = caption
        if var_type is not None:
            self._type = var_type
        BaseTree.__init__(self)

    def get_value(self, env):
        return env.get_variable(self._caption)

    def make_tree(self, env):
        self._clean_tree()
        if not env.has_variable(self._caption):
            _msg = "Err: NameError! No variable with name " + self._caption + "!"
            self._add_error(error.NoNameError(_msg))
            return ResultVariable(False, "NameError in VarLeaf")
        # TODO: move upper validation in validation
        return self.get_value(env)


class NoneLeaf(BaseTree):
    """
    Just none. As your chances to safe your place in the MEPhI
    """
    _operation_num = OperationTypes.NONETYPE

    def __init__(self):
        BaseTree.__init__(self)

    def make_tree(self, env):
        return None

# ==============
# === OUTPUT ===
# ==============


class OutputTree(BaseHalfTree):
    """
    Leaf that can print any expression
    """
    _operation_num = OperationTypes.OUTPUT

    def make_tree(self, env):
        def print_item(item):
            if isinstance(item, ArrVariable):
                print('[', end='')
                first = True
                for one_item in item.value():
                    if not first:
                        print(',', end='')
                    first = False
                    print_item(one_item)
                print(']', end='')
            else:
                print(item.value(), end='')
        self._clean_tree()
        res = self._left.make_tree(env)
        self._add_child_errors(self._left)
        if len(self.errors()) == 0 and self._validation(res, None, env):
            print_item(res)
        print('')
        desc = "Output " + str(res)
        return ResultVariable(True, desc)


class RobotOutputTree(BaseHalfTree):
    """
    Prints robot field
    """
    _operation_num = OperationTypes.OUTPUT

    def __init__(self):
        BaseTree.__init__(self)

    def make_tree(self, env):
        print(env.robot.draw())
        desc = "Output"
        return ResultVariable(True, desc)


class EndlTree(BaseTree):
    """
    Leaf that writes \n in output
    """
    _operation_num = OperationTypes.OUTPUT

    def __init__(self):
        BaseTree.__init__(self)

    def make_tree(self, env):
        print("")
        desc = "Output "
        return ResultVariable(True, desc)


class EnterTree(BaseTree):
    """
    Leaf that make simple input
    """
    _operation_num = OperationTypes.OUTPUT

    def __init__(self):
        BaseTree.__init__(self)

    def make_tree(self, env):
        inp = raw_input("")
        return ResultVariable(True, inp)

# =============
# === ROBOT ===
# =============


class RobotMoveTree(BaseTree):
    """
    Moves robot in some direction
    """
    _operation_num = OperationTypes.ROBOT_MOVE

    def __init__(self, direction):
        BaseTree.__init__(self)
        self._direction = direction

    def make_tree(self, env):
        directions = {
            0: env.robot.action_forw, 1: env.robot.action_right,
            2: env.robot.action_back, 3: env.robot.action_left
        }
        res = directions[self._direction]()
        return Variable(res, Types.BOOL)


class RobotGetTree(BaseTree):
    """
    Gets information about distance to the exit
    """
    _operation_num = OperationTypes.ROBOT_GET

    def __init__(self, direction):
        BaseTree.__init__(self)
        self._direction = direction

    def make_tree(self, env):
        directions = {
            0: env.robot.action_getf, 1: env.robot.action_getr,
            2: env.robot.action_getb, 3: env.robot.action_getl
        }
        res = directions[self._direction]()
        return Variable(res, Types.INT)


class RobotPushTree(BaseTree):
    """
    Pushes and moves robot in some direction
    """
    _operation_num = OperationTypes.ROBOT_PUSH

    def __init__(self, direction):
        BaseTree.__init__(self)
        self._direction = direction

    def make_tree(self, env):
        directions = {
            0: env.robot.action_pushf, 1: env.robot.action_pushr,
            2: env.robot.action_pushb, 3: env.robot.action_pushl
        }
        res = directions[self._direction]()
        return Variable(res, Types.BOOL)


class RobotUndoTree(BaseTree):
    """
    Undo robot's pushing
    """
    _operation_num = OperationTypes.ROBOT_UNDO

    def make_tree(self, env):
        res = env.robot.action_undo()
        return Variable(res, Types.BOOL)


class RobotFinishTree(BaseTree):
    """
    Check that robot found finish
    """
    _operation_num = OperationTypes.ROBOT_FINISH

    def make_tree(self, env):
        res = env.robot.is_finished()
        return Variable(res, Types.BOOL)

