%{
     #include <stdio.h>     
     int yylex(void);     
     void yyerror(const char *); 
 %} 

 %token INTEGER 

 %% 

 program:
          program expr '\n'         { printf("%d\n", $2); }         
                  |          
                  ; 
          expr:         
                  expr '+' term           { $$ = $1 + $3; }         
                  | expr '-' term           { $$ = $1 - $3; }        
                  | term         
                  ; 
          term:         
                  term '*' factor           { $$ = $1 * $3; }        
                  | term '/' factor           { $$ = $1 / $3; }               
                  | factor                
                  ; 
          factor:               INTEGER               
                                | '(' expr ')'        { $$ = $2;}               
                                ; 

%% 

void yyerror(const char *s) {
	fprintf(stderr, "%s\n", s); 
 } 

 int main(void) {     
 	yyparse();     
 	return 0; 
 }