Simple Calculator from Dumin presentations
===================

Compilation and run:
----------

* `flex calc.l`
* `bison -d calc.y`
* `gcc -o calc calc.tab.c lex.yy.c`
* `./calc`

So, now you can type expressions like 1 + 1, 1 + 100 * 5, etc.
