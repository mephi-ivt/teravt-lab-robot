import copy


class Types:
    """
    Allowed variable types
    """

    INT, BOOL, STR = 'INT', 'BOOL', 'STR_S'
    ARR, ARR_ARR, SYS_ARR = 'ARR', 'ARR_ARR', 'ARR_S'
    RESULT, FUNC_DEF, TREE = 'RESULT', 'FUNC_DEF', 'TREE'
    NONE = 0  # STRANGE VERY STRANGE! PLEASE RUN! GO AWAY!
    # '_S' means system

    STANDARD_TYPES = [INT, BOOL, STR]  # three most standard types

    _types_dict = {int: INT, bool: BOOL, list: ARR, str: STR}
    _inv_types_dict = {INT: int, BOOL: bool, ARR: list, STR: str, ARR_ARR: list}

    @staticmethod
    def check(variable):
        """
        Knows type of variable in Types format
        :param variable: 
        :return: Types.* or Types.NONE
        """

        if type(variable) in Types._types_dict:
            return Types._types_dict[type(variable)]
        return Types.NONE


class Variable:
    """
    Base variable class with value and some type from Types
    """

    _value = None
    _type = None

    _is_const = False

    def __init__(self, value, var_type=None, is_const=False):
        """
        :param value: int, bool or str
        :param var_type: Types.* type
        """
        self._value = value
        if var_type is None:
            self._type = Types.check(self._value)
        elif self._type is None:
            self._type = var_type

        self._is_const = is_const

    def value(self):
        return self._value

    def var_type(self):
        """
        Returns type of variable in Types format
        :return: 
        """
        return self._type

    def correct(self):
        """
        Is variable correct
        :return: bool
        """
        if None in [self._value, self._type]:
            return False
        if self._type not in Types.STANDARD_TYPES:
            return False
        return True

    def change(self, value):
        """
        Tries to change variable
        :param value: 
        :return: bool (success)
        """
        if not isinstance(value, type(self._value)):
            return False
        if self._is_const:
            return False
        self._value = value
        return True

    def is_const(self):
        return self._is_const

    def set_const(self):
        self._is_const = True

    def unset_const(self):
        self._is_const = False


class IntVariable(Variable):
    """
    Keep integer value
    """
    _type = Types.INT

    def __init__(self, value):
        Variable.__init__(self, value)


class BoolVariable(Variable):
    """
    Keep boolean value
    """
    _type = Types.BOOL

    def __init__(self, value):
        Variable.__init__(self,  value)


class FreeArrVariable(Variable):
    """
    Array variable consist number of Variables.
    """
    STANDARD_TYPE = Types.INT  # type if value = [] and val_types is None
    ALLOWED_TYPES = []
    MYSELF_TYPE = Types.ARR

    _val_types = None
    _type_check = False

    def __init__(self, values=list(), val_types=None):
        """
        :param values: list of variables
        :param val_types: Types.* type of variables
        """
        Variable.__init__(self, copy.copy(values), self.MYSELF_TYPE)
        if type(values) is not list:
            self._value = []

        if val_types is not None and self._is_type_allowed(val_types):
            self._val_types = val_types

        elif len(self._value) > 0:
            self._val_types = self._value[0].var_type()
        else:
            self._val_types = self.STANDARD_TYPE

    def val_types(self):
        """
        :return: type of array values in Types format
        """
        return self._val_types

    def correct(self):
        if None in [self._value, self._type]:
            return False
        if self._type_check:
            if not self._correct_types():
                return False
        return True

    def _correct_types(self):
        for el in self._value:
            if not self._is_type_correct(el):
                return False
        return True

    def values(self):
        """
        Values is mor understandable in arrays than 'value'
        :return: 
        """
        return self.value()

    def change(self, value_pair):
        if isinstance(value_pair[0], Variable) and value_pair[0].var_type() == Types.ARR:
            arrs_type = value_pair[0].val_types()
            for value in value_pair:
                correctness = isinstance(value, Variable) and value.var_type() == Types.ARR
                type_correctness = value.val_types() == arrs_type
                if not correctness or not type_correctness:
                    arrs_type = None
            if arrs_type is not None:
                if self._val_types == Types.ARR and len(value_pair) > 0:
                    self._value = copy.copy(value_pair)
                    return True
                elif arrs_type == self._val_types:
                    self._value = copy.copy(value_pair[0].values())
                    return True
                return False
        correct_change_value_type = type(value_pair) is list or type(value_pair) is tuple
        if (not correct_change_value_type) or len(value_pair) != 2:
            return False
        pos, val = value_pair
        return self.arr_change(pos, val)

    def arr_change(self, position, value):
        """
        Change value in array with position
        :param position: 
        :param value: 
        :return: bool (success)
        """
        if position >= len(self._value) or not self._is_type_correct(value):
            return False
        else:
            self._value[position] = value
        return True

    def arr_get(self, position):
        """
        Gets value in array with position
        :param position: 
        :return: value or None
        """
        if position >= len(self._value):
            return None
        return self.values()[position]

    def arr_size(self):
        return len(self.values())

    def arr_resize(self, num):
        """
        Resizes the array. If num < array length, do nothing
        :param num: 
        :return: bool (success)
        """
        arr_len = len(self.values())
        if num >= arr_len:
            standard_element = 0
            if self.arr_size() > 0:
                standard_element = copy.copy(self[len(self.values()) - 1])
                if type(standard_element.value()) is list:
                    standard_element = ArrVariable([standard_element[0]], standard_element.val_types())
            for n in range(0, num - arr_len):
                self._value.append(standard_element)
            return True
        else:
            return False

    def arr_append(self, value):
        if self._is_type_correct(value):
            self._value.append(value)

    def arr_extend(self, arr):
        if arr.val_types() == self.val_types() and arr.correct():
            self._value.extend(arr.values())

    def __getitem__(self, key):
        """
        <var>[key]
        """
        return self.arr_get(key)

    # === This methods may be overwritten in children ===

    @staticmethod
    def _get_value_type(value):
        return Types.check(value)

    def _is_type_allowed(self, val_types):
        return val_types in self.ALLOWED_TYPES or not self._type_check

    def _is_type_correct(self, value):
        return value.var_type() == self.val_types() or not self._type_check


class ArrVariable(FreeArrVariable):
    """
    Array variable consist number of INTs, BOOLs or STRs.
    """
    STANDARD_TYPE = Types.INT
    ALLOWED_TYPES = Types.STANDARD_TYPES

    _type_check = True


class ResultVariable(Variable):
    """
    Keeps dict with result of operation:
      'success': bool,
      'info': some additional info or None
    """
    def __init__(self, success=True, info=None):
        value = {'success': success, 'info': info}
        Variable.__init__(self, value, is_const=True)


class FuncDefVariable(Variable):
    """
    Keeps name and
    """
    def __init__(self, name, standard_value):
        value = {'name': name, 'standard_value': standard_value}
        Variable.__init__(self, value, var_type=Types.FUNC_DEF, is_const=True)