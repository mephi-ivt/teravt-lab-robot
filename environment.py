import copy
from variables import Types, Variable, FreeArrVariable
import robot


class Environment:
    """
    Base Environment class for tree.
    Handle all the variables, functions, program stack and other
    """

    robot = robot.RobotEnvironment(5, 5)

    def __init__(self):
        self._vars = {}
        self._functions = {}

    # ============
    # === VARS ===
    # ============

    def add_variable(self, name, var):
        """
        :param name: str
        :param var: Variable 
        :return: bool success
        """
        if not var.correct():
            return False
        self._vars[name] = copy.copy(var)
        return True

    def get_variable(self, name):
        """
        Returns variable with name 'name' or None
        :param name: str
        :return: Variable
        """
        if name in self._vars:
            return copy.copy(self._vars[name])
        return None

    def change_variable(self, name, value):
        """
        Changes variable if it is possible
        :param name: str
        :param value: new value
        :return: bool (was it changed or not) 
        """
        if name in self._vars:
            return self._vars[name].change(value)
        return False

    def change_arr(self, name, index, value):
        if name in self._vars and self._vars[name].var_type() == Types.ARR:
            return self._vars[name].arr_change(index, value)
        return False

    def change_arr_arr(self, name, index1, index2, value):
        if name in self._vars and self._vars[name].var_type() == Types.ARR:
            if index1 < len(self._vars[name].values()):
                return self._vars[name][index1].arr_change(index2, value)
        return False

    def resize_arr(self, name, value):
        if name in self._vars and self._vars[name].var_type() == Types.ARR:
            return self._vars[name].arr_resize(value)
        return False

    def resize_arr_arr(self, name, index, value):
        if name in self._vars and self._vars[name].var_type() == Types.ARR:
            if len(self._vars[name].values()) > index:
                return self._vars[name][index].arr_resize(value)
        return False

    def has_variable(self, name):
        """
        :param name: str
        :return: bool (has this variable or not)
        """
        return name in self._vars

    # custom methods for UINT, CUINT, BOOLEAN and CBOOLEAN

    def add_int(self, name, value):
        var = Variable(value, Types.INT)
        return self.add_variable(name, var)

    def add_cint(self, name, value):
        var = Variable(value, Types.INT, is_const=True)
        return self.add_variable(name, var)

    def add_bool(self, name, value):
        var = Variable(value, Types.BOOL)
        return self.add_variable(name, var)

    def add_cbool(self, name, value):
        var = Variable(value, Types.BOOL, is_const=True)
        return self.add_variable(name, var)

    # =============
    # === FUNCS ===
    # =============

    def add_func(self, out_vars, label, in_vars, action):
        """
        Adds function to the memory
        :param out_vars: ArrVariable with variables for output
        :param label: Variable with str label
        :param in_vars: ArrVariable with variables for input
        :param action: Variable with tree
        :return: bool success
        """
        # I want to do this with class, no zaebalsa
        func = {
            'out_vars': out_vars,
            'in_vars': in_vars,
            'action': action
        }
        label_val = label.value()
        if label_val in self._functions:
            return False
        self._functions[label_val] = func
        return True

    def has_func(self, label):
        """
        Checks if Environment has function with label
        :param label: str
        :return: bool
        """
        return label in self._functions

    def get_func(self, label):
        return self._functions[label]

    def get_funcs(self):
        return self._functions

    def set_funcs(self, functions):
        self._functions = functions

    def make_func(self, label, variables):
        if not self.has_func(label):
            return False
        func = self.get_func(label)
        new_env = Environment()
        new_env.set_funcs(self.get_funcs())
        new_env.robot = self.robot
        counter = 0
        for need_var in func['in_vars'].values():
            need_name = need_var.value()['name']
            if counter < len(variables):
                new_env.add_variable(need_name.value(), variables[counter])
            else:
                new_env.add_variable(need_name.value(), need_var.value()['standard_value'])
            counter += 1
        res = func['action'].value().make_tree(new_env)
        out_values = func['out_vars'].values()
        if len(out_values) == 1:
            out_value = out_values[0].value()
            if new_env.has_variable(out_value['name'].value()):
                return new_env.get_variable(out_value['name'].value())
            return out_value['standard_value']
        elif len(out_values) > 1:
            return_arr = FreeArrVariable()
            for out_value in out_values:
                if new_env.has_variable(out_value.value()['name'].value()):
                    t_val = new_env.get_variable(out_value.value()['name'].value())
                else:
                    t_val = out_value.value()['standard_value']
                return_arr.arr_append(t_val)
            return return_arr
