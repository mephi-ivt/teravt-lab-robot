Simple Robot Interpreteter with Bison and Flex
===================
This interpreteter should help our small cute robot to get out of the maze.

Requirements:
----------
You should have correctly working *Flex* and *Bison*. <br>
To start run.py you need PyBison in your Python 2.7 interpreteter<br>
Programs were tested on *Ubuntu 16.04*.<br>
You can find 100% correct examples in examples/calc/ :) 

