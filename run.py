#!/usr/bin/env python

import sys
import robot_parser

# sys.path.insert(0, '../../build/lib.linux-x86_64-2.7/')

parser = robot_parser.Parser(verbose=0, keepfiles=0, debug=False, field='tests/field.fld')
parser.run(file='tests/robot.rbt')