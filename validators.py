import error
from variables import Types


def type_validator(var_list, var_type):
    """
    Checks that every value from list has allowes type
    :param var_list: list of Variables
    :param var_type: Types.* type
    :return: Error or None
    """
    for count in range(0, len(var_list)):
        var = var_list[count]
        if var.var_type() != var_type:
            msg = "Err: Types mismatch - " + str(count) + " value (" + str(var.value()) + ") should be " \
                  + str(var_type) + " not " + str(var.var_type()) + "!\n"
            return error.TypeMismatchError(msg)
    return None


def one_type_validator(var, var_type):
    """
    Validates just one variable
    :param var: Variable
    :param var_type: Types.* type
    :return: Error or None
    """
    return type_validator([var], var_type)


def two_int_validator(left, right):
    """
    :param left: Variable
    :param right: Variable
    :return: bool (if left and right are INT)
    """
    return type_validator([left, right], Types.INT)


def twod_array_validator(arr, var_type):
    """
    Checks that arr is 2D array and var types are correct
    :param arr: ArrVariable
    :param var_type: Types.* type
    :return: Error or None
    """
    for el in arr.values():
        if el.var_type() is not Types.ARR:
            msg = "Err: 2D array should have 1D array inside! " \
                   + str(el.var_type()) + " is wrong."
            return error.TypeMismatchError(msg)
        else:
            err = type_validator(el.values(), var_type)
            if err is not None:
                return err
    return None


def array_size_validator(arr, size):
    """
    :param arr: ArrVariable
    :param size: int
    :return: Error or None
    """
    if len(arr.values()) < size:
        msg = "Err: Array size is " + str(len(arr.values())) + " - lower than " + str(size)
        return error.ArraySizeError(msg)
    return None


def array_extend_validator(arr, size):
    """
    :param arr: ArrVariable
    :param size: int
    :return: Error or None
    """
    if len(arr.values()) > size:
        msg = "Err: Array size is " + str(len(arr.values())) + " - more than " + str(size)
        return error.ArraySizeError(msg)
    return None


def existance_validator(label, env):
    """
    :param label: str
    :param env: Environment
    :return: Error or None
    """
    if not env.has_variable(label):
        _msg = "Err: NameError! No variable with name " + label + "!"
        return error.NoNameError(_msg)
    return None


def changeable_validator(label, env):
    """
    :param label: str
    :param env: Environment
    :return: Error or None
    """
    if not env.has_variable(label):
        _msg = "Err: NameError! No variable with name " + label + "!"
        return error.NoNameError(_msg)
    val = env.get_variable(label)
    if val.is_const():
        _msg = "Err: Variable " + str(label) + " is const"
        return error.ConstError(_msg)


def arrays_correctness_validator(label, second_arr, env):
    arr = env.get_variable(label)
    if arr.val_types() == Types.ARR:
        types = arr.values()[0].val_types()
        for el in second_arr.values():
            if el.var_type != Types.ARR and el.val_types() != types:
                return error.TypeMismatchError("Err: Try to 2DARRAY = bad")
    elif arr.var_type() == Types.ARR:
        types = arr.val_types()
        for el in second_arr.values():
            if el.val_types() != types:
                return error.TypeMismatchError("Err: Try to 1DARRAY = bad")
    return None


def funcs_def_validator(out_vars, in_vars, label):
    def check_vars(vars, var_desc=""):
        if len(vars.values()) > 0:
            good_type = vars[0].value()['standard_value'].var_type()
            for el in vars.values():
                typer = el.value()['standard_value'].var_type()
                if typer != good_type:
                    _msg = "Err: in func '" + str(label) + "' all " + var_desc \
                           + " vars should be " + str(good_type) + " not " + str(typer)
                    return error.FuncVarsError(_msg)
        return None
    out_vars_check = check_vars(out_vars, "output")
    in_vars_check = check_vars(in_vars, "input")
    for el in [out_vars_check, in_vars_check]:
        if el is not None:
            return el
    return None


def unigetter_validator(label, variables, env):
    if not (env.has_variable(label) or env.has_func(label)):
        _msg = "Err: NameError! No variable or func with name " + label + "!"
        return error.NoNameError(_msg)
    if env.has_func(label):
        func = env.get_func(label)
        in_vars = func['in_vars'].value()
        if len(in_vars) < len(variables):
            _msg = "Err: Too much in-vars in function '" + str(label) + \
                   "'. Need " + str(len(in_vars))
            return error.TooMuchFuncVarsError(_msg)
        good_type = in_vars[0].value()['standard_value'].var_type()
        for one_var in variables:
            if one_var.var_type() != good_type:
                _msg = "Err: in func '" + str(label) + "' all input vars should be "\
                       + str(good_type) + " not " + str(one_var.var_type())
                return error.FuncVarsError(_msg)

    return None
